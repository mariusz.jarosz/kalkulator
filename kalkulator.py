from tkinter import *
import math

class Window(object):

    def dodajDoEkranu(self, liczba):
        self.e.insert(END,liczba)

    def potega(self):
        self.wyrazenie = float(self.e.get())
        self.wynik = float(self.wyrazenie ** 2)
        self.e.delete(0, END)
        self.e.insert(0, self.wynik)

    def pierwiastek(self):
        self.wyrazenie = self.e.get()
        self.wynik = math.sqrt(float(self.wyrazenie))
        self.e.delete(0, END)
        self.e.insert(0, self.wynik)

    def suma(self):
        self.wyrazenie = self.e.get()
        self.wynik = eval(self.wyrazenie)
        self.e.delete(0, END)
        self.e.insert(0, self.wynik)

    def __init__(self, window):
        self.window = window
        self.window.wm_title("Kalkulator")
        self.calc_wn = StringVar()
        self.e = Entry(window, textvariable = self.calc_wn, width = 20, font = "Arial 16", justify = RIGHT)
        self.e.grid(row = 0, column = 0,  columnspan = 5, padx = 10, pady = 10)

        # przyciski
        b1 = Button(window, text = "1", width = 6, height = 6, command = lambda:self.dodajDoEkranu(1))
        b1.grid(row = 3, column = 0)

        b2 = Button(window, text="2", width=6, height=6, command= lambda:self.dodajDoEkranu(2))
        b2.grid(row=3, column=1)

        b3 = Button(window, text="3", width=6, height=6, command= lambda:self.dodajDoEkranu(3))
        b3.grid(row=3, column=2)

        b4 = Button(window, text="4", width=6, height=6, command= lambda:self.dodajDoEkranu(4))
        b4.grid(row=2, column=0)

        b5 = Button(window, text="5", width=6, height=6, command= lambda:self.dodajDoEkranu(5))
        b5.grid(row=2, column=1)

        b6 = Button(window, text="6", width=6, height=6, command= lambda:self.dodajDoEkranu(6))
        b6.grid(row=2, column=2)

        b7 = Button(window, text="7", width=6, height=6, command= lambda:self.dodajDoEkranu(7))
        b7.grid(row=1, column=0)

        b8 = Button(window, text="8", width=6, height=6, command= lambda:self.dodajDoEkranu(8))
        b8.grid(row=1, column=1)

        b9 = Button(window, text="9", width=6, height=6, command=lambda:self.dodajDoEkranu(9))
        b9.grid(row=1, column=2)

        b0 = Button(window, text="0", width=15, height=6, command=lambda:self.dodajDoEkranu(0))
        b0.grid(row=4, column=0, columnspan = 2)

        bcol = Button(window, text=",", width=6, height=6, command=lambda:self.dodajDoEkranu("."))
        bcol.grid(row=4, column=2)

        bplus = Button(window, text="+", width=6, height=6, command=lambda:self.dodajDoEkranu("+"))
        bplus.grid(row=4, column=3)

        bminus = Button(window, text="-", width=6, height=6, command=lambda:self.dodajDoEkranu("-"))
        bminus.grid(row=3, column=3)

        brazy = Button(window, text="*", width=6, height=6, command=lambda:self.dodajDoEkranu("*"))
        brazy.grid(row=2, column=3)

        bdziel = Button(window, text="/", width=6, height=6, command=lambda:self.dodajDoEkranu("/"))
        bdziel.grid(row=1, column=3)

        bwynik = Button(window, text="=", width=6, height=13, command=lambda:self.suma())
        bwynik.grid(row=3, column=4, rowspan = 2)

        bkwadrat = Button(window, text="^2", width=6, height=6, command=lambda:self.potega())
        bkwadrat.grid(row=2, column=4)

        bpierwiastek = Button(window, text="√", width=6, height=6, command=lambda:self.pierwiastek())
        bpierwiastek.grid(row=1, column=4)




window = Tk()
Window(window)
window.mainloop()